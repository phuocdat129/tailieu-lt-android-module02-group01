package dat.admin.tai_lieu_android_module02_group01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Bai8Activity extends AppCompatActivity implements View.OnClickListener{
    Button btn801;
    Intent intentbtn801;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai8);
        // khởi tạo điều khiển
        btn801 = (Button) findViewById(R.id.btn801);
        btn801.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // hành động sau khi click
        intentbtn801 = new Intent(Bai8Activity.this,ThucHanh801Activity.class);
        startActivity(intentbtn801);
    }
}
