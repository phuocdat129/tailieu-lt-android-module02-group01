package dat.admin.tai_lieu_android_module02_group01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Bai6Activity extends AppCompatActivity implements View.OnClickListener{
    Button btnAsset, btnSharedPref, btnMemoryDevice, btnLesson601;
    Intent intentAsset, intentSharedPref, intentMemoryDevice,intentLesson601;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai6);
        // khởi tạo
        btnAsset = (Button) findViewById(R.id.btnAsset);
        btnSharedPref = (Button) findViewById(R.id.btnSharedPref);
        btnMemoryDevice = (Button) findViewById(R.id.btnMemoryDevice);
        btnLesson601 = (Button) findViewById(R.id.btnLesson601);
        // bắt sự kiện click
        btnAsset.setOnClickListener(this);
        btnSharedPref.setOnClickListener(this);
        btnMemoryDevice.setOnClickListener(this);
        btnLesson601.setOnClickListener(this);
    }
        // hành động sau khi click
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAsset:
                // hành động
                intentAsset = new Intent(Bai6Activity.this, AssetActivity.class);
                startActivity(intentAsset);
                break;
            case R.id.btnSharedPref:
                // hành động
                intentSharedPref = new Intent(Bai6Activity.this, SharedPrefActivity.class);
                startActivity(intentSharedPref);
                break;
            case R.id.btnMemoryDevice:
                // hành động
                intentMemoryDevice = new Intent(Bai6Activity.this, MemoryDeviceActivity.class);
                startActivity(intentMemoryDevice);
                break;
            case R.id.btnLesson601:
                // hành động
                intentLesson601 = new Intent(Bai6Activity.this, Lesson601Activity.class);
                startActivity(intentLesson601);
                break;

            default:
                break;
        }
    }
}
