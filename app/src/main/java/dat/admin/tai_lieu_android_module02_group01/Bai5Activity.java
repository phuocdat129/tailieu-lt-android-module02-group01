package dat.admin.tai_lieu_android_module02_group01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Bai5Activity extends AppCompatActivity implements View.OnClickListener{
    Button btnIntentEx, btnIntentIm, btnIntentFil;
    Intent intentbtnIntentEx, intentbtnIntentIm, intentbtnIntentFil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai5);
        // khởi tạo các điều khiển control
        btnIntentEx = (Button) findViewById(R.id.btnIntentEx);
        btnIntentIm = (Button) findViewById(R.id.btnIntentIm);
        btnIntentFil = (Button) findViewById(R.id.btnIntentFil);
        // click các điều khiển control
        btnIntentIm.setOnClickListener(this);
        btnIntentEx.setOnClickListener(this);
        btnIntentFil.setOnClickListener(this);
    }
        // hành động sau khi click
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnIntentEx:
                // hành động
                intentbtnIntentEx = new Intent(Bai5Activity.this,ExIntentActivity.class);
                startActivity(intentbtnIntentEx);
                break;
            case R.id.btnIntentIm:
                // hành động
                intentbtnIntentIm = new Intent(Bai5Activity.this,ImIntentActivity.class);
                startActivity(intentbtnIntentIm);
                break;
            case R.id.btnIntentFil:
                // hành động
                intentbtnIntentFil = new Intent(Bai5Activity.this,FilIntentActivity.class);
                startActivity(intentbtnIntentFil);
                break;

            default:
                break;
        }
    }
}
