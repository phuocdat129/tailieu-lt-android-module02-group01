package dat.admin.tai_lieu_android_module02_group01;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Size;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import dat.admin.tai_lieu_android_module02_group01.Fragments.FilesFragment;

import static android.view.Gravity.CENTER_VERTICAL;
import static android.widget.Toast.*;
import static android.widget.Toast.LENGTH_LONG;

public class ThucHanh801Activity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thuc_hanh801);
        // khởi tạo
        Fragment fragment = new FilesFragment();
        FragmentManager fragManager = getFragmentManager();
        FragmentTransaction fragTrans = fragManager.beginTransaction();
        // dùng framelayout bên dưới chèn vào Main chính của bài thực hành
        fragTrans.replace(R.id.mainbelowthuchanh801layout, fragment);
        fragTrans.commit();
    }

    @Override
    protected void onStart() {
        Context context = getApplicationContext();
        CharSequence text = "on Start";
        int duration = LENGTH_SHORT;

        Toast toast = makeText(context, text, duration);
        toast.show();

        // viết ngắn gọn
        // Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        super.onStart();
    }

    @Override
    protected void onResume() {
        // dùng thông báo toast có layout và vị trí
        thongBao("On Resume");
        // Toast.makeText(getBaseContext(),"onResume",Toast.LENGTH_SHORT).show();
        super.onResume();
    }

    @Override
    protected void onPause() {
        thongBao("On Pause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        thongBao("On Stop");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        thongBao("On Restart");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        thongBao("On Destroy");
        super.onDestroy();
    }

    private void thongBao(String s) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(s);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(CENTER_VERTICAL, 0, 0);
        toast.setDuration(LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

}