package dat.admin.tai_lieu_android_module02_group01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button btn01, btn02, btn03, btn04, btn05, btn06, btn07, btn08, btn09;
    Intent intentbtn01, intentbtn02, intentbtn03, intentbtn04, intentbtn05, intentbtn06, intentbtn07, intentbtn08, intentbtn09;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // khởi tạo các điều khiển control
        //taoControl();
        btn01 = (Button) findViewById(R.id.btn01);
        btn02 = (Button) findViewById(R.id.btn02);
        btn03 = (Button) findViewById(R.id.btn03);
        btn04 = (Button) findViewById(R.id.btn04);
        btn05 = (Button) findViewById(R.id.btn05);
        btn06 = (Button) findViewById(R.id.btn06);
        btn07 = (Button) findViewById(R.id.btn07);
        btn08 = (Button) findViewById(R.id.btn08);
        btn09 = (Button) findViewById(R.id.btn09);
        // hàm bắt sự kiện click các điều khiển control
        //clickControl();
          btn01.setOnClickListener(this);
          btn02.setOnClickListener(this);
          btn03.setOnClickListener(this);
          btn04.setOnClickListener(this);
          btn05.setOnClickListener(this);
          btn06.setOnClickListener(this);
          btn07.setOnClickListener(this);
          btn08.setOnClickListener(this);
          btn09.setOnClickListener(this);
    }
        // hành động sau khi click
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn01:
                // click btn01
                intentbtn01 = new Intent(MainActivity.this,Bai1Activity.class);
                startActivity(intentbtn01);
                break;
            case R.id.btn02:
                // click btn02
                intentbtn02 = new Intent(MainActivity.this,Bai2Activity.class);
                startActivity(intentbtn02);
                break;
            case R.id.btn03:
                // click btn03
                intentbtn03 = new Intent(MainActivity.this,Bai3Activity.class);
                startActivity(intentbtn03);
                break;
            case R.id.btn04:
                // click btn04
                intentbtn04 = new Intent(MainActivity.this,Bai4Activity.class);
                startActivity(intentbtn04);
                break;
            case R.id.btn05:
                // click btn05
                intentbtn05 = new Intent(MainActivity.this,Bai5Activity.class);
                startActivity(intentbtn05);
                break;
            case R.id.btn06:
                // click btn06
                intentbtn06 = new Intent(MainActivity.this,Bai6Activity.class);
                startActivity(intentbtn06);
                break;
            case R.id.btn07:
                // click btn07
                intentbtn07 = new Intent(MainActivity.this,Bai7Activity.class);
                startActivity(intentbtn07);
                break;
            case R.id.btn08:
                // click btn08
                intentbtn08 = new Intent(MainActivity.this,Bai8Activity.class);
                startActivity(intentbtn08);
                break;
            case R.id.btn09:
                // click btn09
                intentbtn09 = new Intent(MainActivity.this,SoucreLopHocActivity.class);
                startActivity(intentbtn09);
                break;

            default:
                break;
        }
    }
}
