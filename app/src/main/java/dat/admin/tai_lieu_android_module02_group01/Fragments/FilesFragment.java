package dat.admin.tai_lieu_android_module02_group01.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import dat.admin.tai_lieu_android_module02_group01.R;

import static android.view.Gravity.CENTER_VERTICAL;
import static android.widget.Toast.LENGTH_LONG;

/**
 * Created by Admin on 11/12/2015.
 */
public class FilesFragment extends Fragment implements View.OnClickListener {
    LinearLayout filesLayout;
    File[][] arrLayersFiles = new File[10][100];
    Button btnNewDir;
    String currentPath;
    int i = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_files, null);
        filesLayout = (LinearLayout) view.findViewById(R.id.folderLayout);
        btnNewDir = (Button) view.findViewById(R.id.btnNewFolder);
        btnNewDir.setOnClickListener(this);
        arrLayersFiles[i] = getExternalStorage();
        for (File file : getExternalStorage()) {
            Button button = createFilesButton(file);
            filesLayout.addView(button);
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNewFolder:
                newDirectory();
                break;
        }
    }

    private void newDirectory() {
        String folderName = "New Folder";
        Button button = createButton(folderName);
        File file = new File(currentPath, folderName);
        if (isStorageWritable()) {
            int i = 0;
            while (file.exists()) {
                i++;
                file = new File(currentPath, folderName + i);
                button = createButton(folderName + i);
            }
            file.mkdir();
            filesLayout.addView(button);
        }
    }

    private void setNameForButtonAndAddToFileLayout() {
        filesLayout.removeAllViews();
        if (i > 0) {
            Button btnBack = createButton("Back");
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    i--;
                    setNameForButtonAndAddToFileLayout();
                    String[] arrDir = currentPath.split("/");
                    currentPath = "";
                    for (int j = 1; j < arrDir.length - 1; j++) {
                        currentPath += "/" + arrDir[j];
                    }
                    Toast.makeText(getActivity(), currentPath, Toast.LENGTH_SHORT).show();

                }
            });
            filesLayout.addView(btnBack);
        }
        for (File file : arrLayersFiles[i]) {
            Button button = createFilesButton(file);
            filesLayout.addView(button);
        }
    }

    private Button createFilesButton(final File file) {
        Button button = createButton(file.getName());
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i++;
                currentPath = file.getAbsolutePath();
                arrLayersFiles[i] = file.listFiles();
                setNameForButtonAndAddToFileLayout();
                Toast.makeText(getActivity(), currentPath, Toast.LENGTH_SHORT).show();
            }
        });
        return button;
    }

    private Button createButton(String name) {
        LinearLayout.LayoutParams buttonType = new LinearLayout.LayoutParams
                (300, LinearLayout.LayoutParams.WRAP_CONTENT);
        Button button = new Button(getActivity());
        button.setText(name);
        button.setLayoutParams(buttonType);
        return button;
    }

    private File[] getExternalStorage() {
        if (isStorageReadable()) {
            File externalStorage = Environment.getExternalStorageDirectory();
            currentPath = externalStorage.getAbsolutePath();
            return externalStorage.listFiles();
        } else
            return null;
    }

    private boolean isStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
            return true;
        return false;
    }

    private boolean isStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
            return true;
        return false;
    }

    private void addFragmentToLayout() {
        Fragment fragment = new FilesFragment();
        FragmentManager fragManager = getFragmentManager();
        FragmentTransaction fragTrans = fragManager.beginTransaction();
        fragTrans.add(R.id.homeLayout, fragment);
        fragTrans.addToBackStack("");
        fragTrans.commit();
    }

}
